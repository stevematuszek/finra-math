package org.finra.math;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * EndableBlockingQueue is a LinkedBlockingQueue<String> that adds 
 * the ability for the producer to signal that there will be no further
 * input with putEndSignal(), and the consumer(s) to check that signal
 * with isEnded().
 * <P>
 * It is not type-parameterized, but we could implement that by allowing
 * the constructor to specify the value of the end signal.
 *
 */
public class EndableBlockingQueue extends LinkedBlockingQueue<String> {
	
	private static final long serialVersionUID = -4406554097388489698L;
	
	//  The producer puts this on the queue to signal to the consumer
	//  that there are no more problems to solve.
	static final String POISON_PILL = "No more math problems today!";

	/**
	 * Use to signal that there will be nothing more added to the queue.
	 * @throws InterruptedException
	 */
	public void putEndSignal() throws InterruptedException {
		put(POISON_PILL);
	}

	/**
	 * Use to check whether the queue has been exhausted AND the producer
	 * has sent the end signal. Since the end signal goes onto the queue,
	 * this will not return true until every real value has been taken
	 * from the queue.
	 * @return true if the only element in the queue is the end signal.
	 */
	public boolean isEnded() {
		return POISON_PILL.equals(peek());
	}
	
	/**
	 * Use to check whether a given value represents the end signal.
	 * It is preferable for consumers to use isEnded().
	 * @param string The string value to compare to the end signal.
	 * @return true if the the string value is equal to the end signal.
	 */
	public boolean isEndSignal(String string) {
		return POISON_PILL.equals(string);
	}
}
