package org.finra.math;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

/**
 * DoMath takes an input file containing addition/subtraction problems 
 * and outputs a file containing their solutions.
 *
 */
public class DoMath {

	Logger logger = Logger.getLogger(DoMath.class.getName());
	
	String infileName;
	String outfileName;

	EndableBlockingQueue problemQueue;
	
	public DoMath(String infileName, String outfileName) {
		this.infileName  = infileName;
		this.outfileName = outfileName;
		this.problemQueue = new EndableBlockingQueue();
	}

	private void go() throws Exception {
		
		MathReader reader = new MathReader(infileName, problemQueue);
		MathWriter writer = new MathWriter(outfileName, problemQueue);

		ExecutorService executor = Executors.newFixedThreadPool(2);
		
		//  Start the writer first, since it is the consumer 
		//  (it writes to a file, but it READS the shared queue).
		//  Because we do not need to examine its status, we 
		//  just use execute rather than submit.
		logger.info("Starting the writer (consumer) thread.");
		executor.execute(writer);

		//  Then start the reader, which is the producer.
		//  We do need to examine its status, so we will use
		//  the submit method.
		logger.info("Starting the reader (producer) thread.");
		Future<?> readerStatus = executor.submit(reader);
		
		//  Blocks until the producer is finished.
		logger.info("Waiting for the reader thread to finish.");
		readerStatus.get();
		logger.info("The reader thread is finished.");
		
		//  Shut the ExecutorService down cleanly, 
		//  allowing all its threads to finish first.
		//  The consumer thread won't finish until it has 
		//  read the entire queue.
		logger.info("Sending shutdown signal.");
		executor.shutdown();
		logger.info("Shutdown signal sent. Outstanding tasks should still complete.");
	}

	public static void main(String[] args) throws Exception {

		if (args.length != 2) {
			System.out.println("Usage: java DoMath <full-path-to-input-file> <full-path-to-output-file>");
			return;
		}
		
		String infileName  = args[0];
		String outfileName = args[1];
		
		new DoMath(infileName, outfileName).go();
	}
}

class MathReader implements Runnable {

	private String filename;
	private EndableBlockingQueue problemQueue;

	public MathReader(String infileName, EndableBlockingQueue problemQueue) {
		this.filename = infileName;
		this.problemQueue = problemQueue;
	}

	@Override
	public void run() {
		try {
			read();
		} catch (Exception e) {
			//  A stand-in for more extensive logging.
			System.err.println("Encountered a problem reading: " + e.getMessage());
		}
	}
	
	private void read() throws Exception {
		
		//  Allowing exceptions to bubble all the way up, for now.
		File infile = new File(filename);
		BufferedReader reader = new BufferedReader(new FileReader(infile));

		String line;
		while ((line = reader.readLine()) != null) {
			problemQueue.put(line);
		}
		problemQueue.putEndSignal();

		reader.close();
	}
}

class MathWriter implements Runnable {
	
	private String filename;
	private EndableBlockingQueue problemQueue;

	public MathWriter(String outfileName, EndableBlockingQueue problemQueue) {
		this.filename = outfileName;
		this.problemQueue = problemQueue;
	}

	@Override
	public void run() {
		try {
			write();
		} catch (Exception e) {
			//  A stand-in for more extensive logging.
			System.err.println("Encountered a problem writing: " + e.getMessage());
		}
	}
	
	private void write() throws Exception {

		File outfile = new File(filename);
		PrintWriter writer = new PrintWriter(outfile);
		
		while (!problemQueue.isEnded()) {
			
			String problem = problemQueue.take();
			
			int result = ExpressionEvaluator.evaluate(problem);
			
			writer.println(problem + " = " + result);
			writer.flush();
			
			//  Used to test that the executor service really does wait
			//  for this thread to finish.
			//Thread.sleep(1000);
		}
		writer.close();
	}
}

