package org.finra.math;

import java.util.StringTokenizer;

public class ExpressionEvaluator {

	/**
	 * Takes a String representing an integer addition and/or subtraction expression, 
	 * and returns the result.
	 * <P>
	 * The input String may be of any length, should involve only integers, and 
	 * should have spaces in between each token, e.g., "1 + 4 - 3".
	 * 
	 * @param input The String to evaluate.
	 * @return The result of the evaluation.
	 */
	public static int evaluate(String input) {

		String delimiter = " ";
		StringTokenizer tokenizer = new StringTokenizer(input, delimiter);

		//  Assumption: expression has at least one token
		String firstToken = tokenizer.nextToken();

		//  Assumption: safe to assume that the first value is a number.
		int runningTotal = Integer.parseInt(firstToken);
		
		while (tokenizer.hasMoreTokens()) {
			String nextOperator = tokenizer.nextToken();
			String nextOperand  = tokenizer.nextToken();
			
			int operandValue = Integer.parseInt(nextOperand);
			
			if (nextOperator.equals("+")) {
				runningTotal += operandValue;
			} else if (nextOperator.equals("-")) {
				runningTotal -= operandValue;
			}
		}
		
		return runningTotal;
	}

}
