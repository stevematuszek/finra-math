package org.finra.math;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ParameterizedTest {

	private final String input;
	private final int expected;

	public ParameterizedTest(String input, int expected) {
		this.input    = input;
		this.expected = expected;
	}
	
    @Parameters(name="{index}: {0} = {1}")
    public static Iterable<Object[]> data() {
    	
    	//  The contract of a Parameterized JUnit test is that a data
    	//  method supplies an array of arrays, in which the elements of
    	//  the outer array each represent one test case, and the elements
    	//  of the inner array represent the values to be passed to the 
    	//  class's constructor for that test case. The runner runs every
    	//  @Test method on the classes created in this way. 
    	
    	return Arrays.asList(new Object[][] {
    		{ "1 + 1", 2 },
    		{ "2 + 2", 4 },	
    		{ "3 + 1 - 2", 2 },
    		{ "2 + 3 + 2", 7 },
    		{ "1 + 3 + 2 + 9", 15 },
    	});
    }

    @Test
	public void testEvaluation() {		
		int actual = ExpressionEvaluator.evaluate(this.input);
		Assert.assertEquals("Evaluation error.", this.expected, actual);
	}

}
