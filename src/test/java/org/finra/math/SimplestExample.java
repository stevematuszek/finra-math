package org.finra.math;

import org.junit.Assert;
import org.junit.Test;

public class SimplestExample {

	@Test
	public void onePlusOne() {
		
		String input = "1 + 1";
		int expected = 2;
		int actual   = ExpressionEvaluator.evaluate(input);
		
		Assert.assertEquals("Evaluation error.", expected, actual);
	}
	
	
}
